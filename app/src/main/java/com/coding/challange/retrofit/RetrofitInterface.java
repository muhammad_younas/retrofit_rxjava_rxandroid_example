package com.coding.challange.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface RetrofitInterface {
    // https://api.github.com/users?since=0&per_page=20
    @GET("users?")
    Call<List<RetrofitModelClass>> getUsers(@Query("since") Integer start,
                                            @Query("per_page") Integer end);
    @GET("users")
    Call<RetrofitModelClass> getUserById(@Query("id") Integer id);

    @GET("users?")
    Observable<List<RetrofitModelClass>> getUsersObsevable(@Query("since") Integer start,
                                                           @Query("per_page") Integer end);
}

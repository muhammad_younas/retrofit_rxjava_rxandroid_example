package com.coding.challange.retrofit;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetrofitBuilder implements Callback<List<RetrofitModelClass>> {
    static final String BASE_URL = "https://api.github.com/";
    RetrofitInterface allusers;

    @Override
    public void onResponse(Call<List<RetrofitModelClass>> call, Response<List<RetrofitModelClass>> response) {
        if (response.isSuccessful()) {
            List<RetrofitModelClass> changesList = response.body();
            for (int i = 0; i < changesList.size(); i++) {
                Log.d("TAG", "" + changesList.get(i).getId() + "--" + changesList.get(i).getLogin());
            }
        } else {
            System.out.println(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<List<RetrofitModelClass>> call, Throwable t) {
        t.printStackTrace();
    }

    public void start(int start, int user_per_page) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        allusers = retrofit.create(RetrofitInterface.class);
        Call<List<RetrofitModelClass>> call = allusers.getUsers(start, user_per_page);
        call.enqueue(this);
    }
}

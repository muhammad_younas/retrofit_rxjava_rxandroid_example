package com.coding.challange;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.coding.challange.retrofit.RetrofitInterface;
import com.coding.challange.rxjava.GitHubRepoAdapter;
import com.coding.challange.rxjava.RxJavaBuilder;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    // Trailing slash is needed
    int start = 0;
    int per_page =5;
    int end = per_page;
    private GitHubRepoAdapter adapter = new GitHubRepoAdapter();
    private Subscription subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ListView listView = (ListView) findViewById(R.id.list_view_repos);
        listView.setAdapter(adapter);

    }

    public void fetchUsers(View view) {
//        RetrofitBuilder controller = new RetrofitBuilder();
//        controller.start(start,end);

        fetchPeopleList(start,end); // start =0, end = 5
        start = end +1; // 6
        end = end + per_page; // end =11;

    }

//    private void getStarredRepos() {
//        subscription = RxJavaBuilder.getInstance()
//                .getStarredRepos()
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Observer<List<RetrofitModelClass>>() {
//                    @Override public void onCompleted() {
//                        Log.d(TAG, "In onCompleted()");
//                    }
//
//                    @Override public void onError(Throwable e) {
//                        e.printStackTrace();
//                        Log.d(TAG, "In onError()");
//                    }
//
//                    @Override public void onNext(List<RetrofitModelClass> gitHubRepos) {
//                        Log.d(TAG, "In onNext()");
//                        adapter.setGitHubRepos(gitHubRepos);
//                    }
//                });
//    }

//    private void fetchPeopleList() {
//        final String URL = "http://api.randomuser.me/?results=15&nat=en";
//        RxJavaBuilder service = RxJavaBuilder.getInstance();
//        Subscription subscription;
//        subscription = service.getStarredRepos()
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn((AndroidSchedulers.mainThread())
//                .subscribe(new Action1<RetrofitModelClass>() {
//                    @Override
//                    public void call(RetrofitModelClass response) {
//
//
//                    }
//                }, new Action1<Throwable>() {
//                    @Override
//                    public void call(Throwable throwable) {
//                        throwable.printStackTrace();
//                        Log.d("TAG","error in resposnse");
//                    }
//                }));
//
//
//    }

    private void fetchPeopleList(int start,int end) {
        RxJavaBuilder mTEmp = new RxJavaBuilder();
        RetrofitInterface service = mTEmp.gitHubService;
        subscription = service.getUsersObsevable(start,end)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(retrofitModelClasses -> {
                    Toast.makeText(MainActivity.this, ""+retrofitModelClasses.size(),
                            Toast.LENGTH_SHORT).show();
                    adapter.setGitHubRepos(retrofitModelClasses);

                }, throwable -> {

                });
    }

    @Override protected void onDestroy() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
        super.onDestroy();
    }
}
